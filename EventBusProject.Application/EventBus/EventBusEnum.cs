﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBusProject.Application
{
    public enum EventBusEnum
    {
        /// <summary>
        /// 新增工程
        /// </summary>
        [Description("新增工程")]
        ProjectInsert,
        /// <summary>
        /// 更新工程
        /// </summary>
        [Description("更新工程")]
        ProjectUpdate
    }
}
