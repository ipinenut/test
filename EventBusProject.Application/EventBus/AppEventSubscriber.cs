﻿using Furion.DependencyInjection;
using Furion.EventBus;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace EventBusProject.Application
{
    public class AppEventSubscriber : IEventSubscriber, ISingleton
    {
        private readonly ILogger<AppEventSubscriber> _logger;

        public AppEventSubscriber(ILogger<AppEventSubscriber> logger)
        {
            _logger = logger;
        }

        [EventSubscribe(EventBusEnum.ProjectInsert)]
        public void ProjectInsert(EventHandlerExecutingContext context)
        {

        }

        [EventSubscribe(EventBusEnum.ProjectUpdate)]
        public async Task ProjectUpdate(EventHandlerExecutingContext context)
        {
            var projectId = (long)context.Source.Payload;
            await Task.CompletedTask;
        }
    }
}
