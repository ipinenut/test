﻿namespace EventBusProject.Application;

public interface ISystemService
{
    string GetDescription();
}
