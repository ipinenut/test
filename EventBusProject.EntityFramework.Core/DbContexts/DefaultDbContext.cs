﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace EventBusProject.EntityFramework.Core;

[AppDbContext("EventBusProject", DbProvider.Sqlite)]
public class DefaultDbContext : AppDbContext<DefaultDbContext>
{
    public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
    {
    }
}
