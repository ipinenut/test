﻿using Furion;
using Microsoft.Extensions.DependencyInjection;

namespace EventBusProject.EntityFramework.Core;

public class Startup : AppStartup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDatabaseAccessor(options =>
        {
            options.AddDbPool<DefaultDbContext>();
        }, "EventBusProject.Database.Migrations");
    }
}
