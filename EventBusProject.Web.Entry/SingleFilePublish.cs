﻿using Furion;
using System.Reflection;

namespace EventBusProject.Web.Entry;

public class SingleFilePublish : ISingleFilePublish
{
    public Assembly[] IncludeAssemblies()
    {
        return Array.Empty<Assembly>();
    }

    public string[] IncludeAssemblyNames()
    {
        return new[]
        {
            "EventBusProject.Application",
            "EventBusProject.Core",
            "EventBusProject.EntityFramework.Core",
            "EventBusProject.Web.Core"
        };
    }
}